/*
 * Copyright 2024 bsodkpanic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <irrlicht/irrlicht.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

int main(void)
{
	IrrlichtDevice *device = createDevice(EDT_NULL, dimension2d<u32>(1, 1), 0, false, false, false, 0);
	if (!device) return 1;

	IVideoModeList *modes = device->getVideoModeList();
	dimension2d<u32> desktopResolution = modes->getDesktopResolution();
	s32 desktopDepth = modes->getDesktopDepth();
	device->drop();

	device = createDevice(EDT_OPENGL, desktopResolution, desktopDepth, true, false, false, 0);
	if (!device) return 1;

	device->setWindowCaption(L"LANPartyScrabble");

	IVideoDriver *driver = device->getVideoDriver();
	ISceneManager *smgr = device->getSceneManager();
	IGUIEnvironment *guienv = device->getGUIEnvironment();

	int lastFPS = -1;

	while (device->run())
	{
		driver->beginScene(true, true, SColor(0, 0, 0, 0));
		smgr->drawAll();
		guienv->drawAll();
		driver->endScene();
	}

	device->drop();
	return 0;
}
